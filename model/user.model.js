const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    login: String,
    password: String,
    roles: [String],
    name: String
})

module.exports = mongoose.model('User', userSchema)