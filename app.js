const express = require('express')
const cors = require('cors')
const userRoute = require('./routes/user.routes')
const testRoute = require('./routes/test.routes')
const authInterceptor = require('token-api/interceptor/auth.interceptor')

const app = express()

app.use(cors())
app.use(express.json())

authInterceptor(app)

userRoute(app)
testRoute(app)

app.listen(8081, () => {
    console.log('Server is up!')
})