const db = require("../db")
const User = require("../model/user.model")

exports.findByLogin = async (userLogin) => {
    let con = await db.connect()

    try {
        let user = await User.findOne({ login: userLogin })
        return user
    } catch(error) {
        console.log(error)
        return null
    } finally {
        con.disconnect()
    }
}