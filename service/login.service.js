const bcrypt = require('bcrypt')
const repository = require('../repository/user.repository')
const authHelper = require('token-api/helper/auth.helper')

exports.login = async (loginData) => {

    let user = await repository.findByLogin(loginData.login)

    if (user == null) {
        throw new Error("Dados inválidos para o login")
    } else {
        let isPasswordValid = bcrypt.compareSync(loginData.password, user.password)

        if (isPasswordValid) {
            let token = authHelper.createToken(user)
            
            return {
                user: user.name,
                roles: user.roles,
                token: token
            }
        } else {
            throw new Error("Dados inválidos para o login")
        }
    }
}