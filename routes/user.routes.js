const loginService = require('../service/login.service')

module.exports = (app) => {

    app.post('/user/login', async (req, res) => {
        try {
            let loginResult = await loginService.login(req.body)
            res.json(loginResult)
        } catch(error) {
            res.status(401)
            res.statusMessage = error.message
            res.end()
        }
    })

}